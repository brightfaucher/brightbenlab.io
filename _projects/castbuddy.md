---
title: "CastBuddy"
excerpt: "Orthopedic cast monitoring system"
---

CastBuddy was the capstone project I worked on during my final semester at Northeastern University. The grand-scheme goal of the project was a complete system to monitor the conditions inside orthopedic casts in order to detect problems more accurately than cast patients would notice them, and alert a physician to a potential problem. In practice, we managed to build a complete vertical prototype that:

 1. Monitors pressure inside the cast via pressure sensors designed by us and a battery-powered microprocessor
 1. Sends pressure data to a database in the cloud via cellular communications
 1. Displays raw pressure data in a front-end web app

I took charge of the web software side of the project, which included a rest API that could both:

 1. Be accessed by the device to store data in a database
 1. Be accessed by the front-end to retrieve data points

and the front-end web app.